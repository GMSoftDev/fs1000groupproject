const gbtns = document.querySelectorAll('div.Category-btn button');
const gallery = document.querySelectorAll('div.gbox');
//const logout = document.getElementById('adLogout');
const galfunc = document.getElementsByClassName('gallery-func');

const stars = document.querySelector(".g-btn.starsbnt");
const planets = document.querySelector(".g-btn.planets");
const mysteries = document.querySelector(".g-btn.mysteries");
const heading = document.querySelector(".g-info-heading.stars-info");
const para = document.querySelector(".g-info-text.stars-text");

stars.addEventListener('click', function() {
    heading.textContent = "STARS";
    para.textContent = "Space in Pictures: Stars";
})

planets.addEventListener('click', function() {
    heading.textContent = "PLANETS";
    para.textContent = "Space in Pictures: Planets";
})

mysteries.addEventListener('click', function() {
    heading.textContent = "MYSTERIES";
    para.textContent = "Space in Pictures: Mysteries";
})

window.addEventListener('load',adminLoad());
/*logout.addEventListener('click',function ()
{     
    window.location.href="adminLogin.html";
})*/


gbtns.forEach(btn => {
    btn.addEventListener('click', categoryChange); 
 });

 
 function adminLoad(event)
 {
    gallery.forEach(gbox => {
        const image = gbox.getElementsByTagName('img');
        image[0].style.display="none";
        image[0].parentElement.style.display ="none";
    })     
    galfunc[0].style.display="none";
 }

 function categoryChange(event){
    console.log( event.target.id );
    gallery.forEach(gbox => {
        let image = gbox.getElementsByTagName('img');
        let src = image[0].src.toLowerCase();
        let str="photo";
        str=str.concat(event.target.id.slice(0,4));
        if(src.includes(str))
        {
            image[0].style.display="block";
            image[0].parentElement.style.display ="block";
        }
        else
        {
            image[0].style.display="none";
            image[0].parentElement.style.display ="none";
        }
    }) 
    galfunc[0].style.display="block";
    galfunc[0].setAttribute("align", "center");
 }

 

